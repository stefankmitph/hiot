package stefankmitph.hiot;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import stefankmitph.model.DatabaseManager;
import stefankmitph.model.Word;

public class MainActivity extends AppCompatActivity implements ActivityObjectProvider {

    private SQLiteDatabase database;
    private Bundle bundle;
    private String book;
    private int chapter;
    private int verse;
    private HashMap<Integer, List<Word>> m_map;
    private Typeface m_hebrew_typeface;
    private Typeface m_standard_typeface;
    private MyPagerAdapter m_pagerAdapter;
    private Toolbar m_toolbar;
    private TextView m_toolbarTitle;
    private ActionBarDrawerToggle m_drawerToggle;
    private ListView m_drawerList;
    private DrawerListAdapter m_adapter;

    private static final int RESULT_SETTINGS = 1;

    public void setActionBarTitle(String title) {
        m_toolbarTitle.setText(title);
    }

    private void initializeData(List<Word> words) {
        m_map = new HashMap<>();
        for (Word word : words) {
            if (!m_map.containsKey(word.getVerse()))
                m_map.put(word.getVerse(), new ArrayList<Word>());

            m_map.get(word.getVerse()).add(word);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        final Intent intent = getIntent();
        if (intent.getExtras() != null) {
            final String book = intent.getStringExtra("book");
            final int chapter = Integer.valueOf(intent.getStringExtra("chapter"));
            final int verse = Integer.valueOf(intent.getStringExtra("verse"));


            SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(this);

            final SharedPreferences.Editor edit = sharedPrefs.edit();
            edit.putString("book", book);
            edit.putInt("chapter", chapter);
            edit.putInt("verse", verse);
            edit.apply();

            loadData(book, chapter, verse);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer);

        m_toolbar = (Toolbar) findViewById(R.id.tool_bar);

        m_toolbarTitle = (TextView) findViewById(R.id.toolbar_title);

        setSupportActionBar(m_toolbar);

        m_drawerList = (ListView) findViewById(R.id.nav_list);

        String[] items = {"select", "find", "bookmarks", "settings"};
        Integer[] images =
                {
                        R.drawable.ic_open_in_new_black_24dp,
                        R.drawable.ic_search_black_24dp,
                        R.drawable.ic_bookmark_border_black_24dp,
                        R.drawable.ic_settings_black_24dp };

        m_adapter = new DrawerListAdapter(this, items, images);
        m_drawerList.setAdapter(m_adapter);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        m_drawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                m_toolbar,
                R.string.drawer_open,
                R.string.drawer_close
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        m_drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                                    View menu = view;
                                                    String text = (String)((TextView)menu.findViewById(R.id.drawer_item_textview)).getText();
                                                    if(text.toLowerCase() == "select") {
                                                        Intent myIntent = new Intent(getApplicationContext(), SelectBookActivity.class);
                                                        startActivity(myIntent);
                                                    }
                                                }
                                            }
        );

        drawerLayout.addDrawerListener(m_drawerToggle);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        m_hebrew_typeface = Typeface.createFromAsset(this.getAssets(), "fonts/Cardo104s.ttf");

        m_standard_typeface = Typeface.createFromAsset(this.getAssets(), "fonts/LibreBaskerville-Regular.ttf");


        DatabaseManager.init(this);

        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);

        book = sharedPrefs.getString("book", "Genesis");
        chapter = sharedPrefs.getInt("chapter", 1);
        verse = sharedPrefs.getInt("verse", 1);

        loadData(book, chapter, verse);
    }

    private void loadData(String book, int chapter, int verse) {
        DatabaseManager manager = DatabaseManager.getInstance();
        List<Word> words = manager.getChapter(book, chapter);

        initializeData(words);

        m_toolbarTitle.setText(String.format("%s %d:%d", book, this.chapter, verse));


        m_pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

        final ViewPager myPager = (ViewPager) findViewById(R.id.home_panels_pager);
        myPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
        myPager.setOffscreenPageLimit(5);
        myPager.setAdapter(m_pagerAdapter);
        myPager.setCurrentItem(verse - 1);
        myPager.setOnPageChangeListener(new CircularViewPagerHandler(myPager));
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        m_drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        m_drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public SQLiteDatabase getDatabase() {
        return this.database;
    }

    @Override
    public List<Word> getWords(int verse) {
        assert m_map != null;
        if (m_map.containsKey(verse))
            return m_map.get(verse);
        return null;
    }

    @Override
    public HashMap<Integer, List<Word>> getChapter() {
        assert m_map != null;
        return m_map;
    }

    public Typeface getM_hebrew_typeface() {
        return m_hebrew_typeface;
    }

    public Typeface getM_standard_typeface() {
        return m_standard_typeface;
    }

    @Override
    public Bundle getPreferences() {

        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);

        Bundle bundle = new Bundle();
        bundle.putBoolean("show_strongs", sharedPrefs.getBoolean("show_strongs", true));
        bundle.putBoolean("show_concordance", sharedPrefs.getBoolean("show_concordance", true));
        bundle.putBoolean("show_transliteration", sharedPrefs.getBoolean("show_transliteration", true));
        bundle.putBoolean("show_lemma", sharedPrefs.getBoolean("show_lemma", true));
        bundle.putString("font_size_word", sharedPrefs.getString("font_size_word", "2"));

        return bundle;
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return VerseFragment.newInstance(book, chapter, position + 1);
        }

        @Override
        public int getCount() {
            return m_map.keySet().size(); // myPager -> scroll
        }

        private String getFragmentName(int viewPagerId, int index) {
            return "android:switcher:" + viewPagerId + ":" + index;
        }
    }

    public class CircularViewPagerHandler implements ViewPager.OnPageChangeListener {
        private ViewPager mViewPager;
        private int mCurrentPosition;
        private int mScrollState;

        public CircularViewPagerHandler(final ViewPager viewPager) {
            mViewPager = viewPager;
        }

        @Override
        public void onPageSelected(final int position) {
            mCurrentPosition = position;
        }

        @Override
        public void onPageScrollStateChanged(final int state) {
            handleScrollState(state);
            mScrollState = state;
        }

        private void handleScrollState(final int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
                setNextItemIfNeeded();
            }
        }

        private void setNextItemIfNeeded() {
            if (!isScrollStateSettling()) {
                handleSetNextItem();
            }
        }

        private boolean isScrollStateSettling() {
            return mScrollState == ViewPager.SCROLL_STATE_SETTLING;
        }

        private void handleSetNextItem() {
            final int lastPosition = mViewPager.getAdapter().getCount() - 1;
            if (mCurrentPosition == 0) {
                mViewPager.setCurrentItem(lastPosition, false);
            } else if (mCurrentPosition == lastPosition) {
                mViewPager.setCurrentItem(0, false);
            }
        }

        @Override
        public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        android.support.v7.app.ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeButtonEnabled(false); // disable the button
            supportActionBar.setDisplayHomeAsUpEnabled(false); // remove the left caret
            supportActionBar.setDisplayShowHomeEnabled(false); // remove the icon
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (m_drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, RESULT_SETTINGS);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SETTINGS:
                reloadFragment();
                break;

        }
    }

    private void reloadFragment() {
        ViewPager pager = (ViewPager) findViewById(R.id.home_panels_pager);
        String fragmentId = m_pagerAdapter.getFragmentName(R.id.home_panels_pager, pager.getCurrentItem());

        int currentItem = pager.getCurrentItem();
        pager.setAdapter(m_pagerAdapter);
        pager.setCurrentItem(currentItem);
    }

}
