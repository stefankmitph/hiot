package stefankmitph.hiot;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import stefankmitph.model.BookNavigator;
import stefankmitph.model.DatabaseManager;

public class SelectBookActivity extends AppCompatActivity {

    private SQLiteDatabase database;
    private BookNavigator navigator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_book);

        DatabaseManager.init(this);
        DatabaseManager instance = DatabaseManager.getInstance();

        navigator = new BookNavigator(database);

        final ListView listView = (ListView) findViewById(R.id.listView_book);

        String[] books = navigator.getBooks();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.list_selection_item, R.id.list_item_textview, books);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String book = (String)listView.getItemAtPosition(position);

                Intent myIntent = new Intent(getApplicationContext(), SelectChapterActivity.class);
                myIntent.putExtra("book", book);
                startActivity(myIntent);
            }
        });
    }
}
