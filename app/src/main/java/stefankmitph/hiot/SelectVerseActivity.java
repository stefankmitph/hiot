package stefankmitph.hiot;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import stefankmitph.model.BookNavigator;
import stefankmitph.model.DatabaseManager;

public class SelectVerseActivity extends AppCompatActivity {

    private SQLiteDatabase database;
    private BookNavigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_verse);

        Intent intent = getIntent();
        final String book = intent.getStringExtra("book");
        final String chapter = intent.getStringExtra("chapter");

        DatabaseManager.init(this);
        DatabaseManager instance = DatabaseManager.getInstance();

        navigator = new BookNavigator(database);

        final ListView listView = (ListView) findViewById(R.id.listView_verse);

        final List<String> verses = navigator.getVerses(book, Integer.valueOf(chapter));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, verses);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String verse = (String)listView.getItemAtPosition(position);

                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                myIntent.putExtra("book", book);
                myIntent.putExtra("chapter", chapter);
                myIntent.putExtra("verse", verse);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                finish();
                startActivity(myIntent);
            }
        });
    }
}
