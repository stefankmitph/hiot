package stefankmitph.hiot;

import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;

import java.util.HashMap;
import java.util.List;

import stefankmitph.model.Word;

/**
 * Created by KumpitschS on 14.04.2015.
 */
public interface ActivityObjectProvider {
    public SQLiteDatabase getDatabase();

    List<Word> getWords(int verse);

    HashMap<Integer, List<Word>> getChapter();

    Typeface getM_hebrew_typeface();

    Typeface getM_standard_typeface();

    Bundle getPreferences();
}
